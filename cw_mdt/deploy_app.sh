#!/bin/bash
user="$(whoami)"
proj_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
django_proj_name="cw_mdt"
python_env="mdt_env"
# Configure a Python Virtual Environment
sudo rm -r $proj_dir/$python_env
cd $proj_dir && virtualenv $python_env
# Install all python dependencies
$proj_dir/$python_env/bin/pip install django==1.11.2
$proj_dir/$python_env/bin/pip install numpy==1.12
$proj_dir/$python_env/bin/pip install pygdal==1.11.3.3
$proj_dir/$python_env/bin/pip install requests==2.11.1
# Install all frontend dependencies
sudo rm -r $proj_dir/assets/bower_components
cd $proj_dir/assets && bower install
# Configure Apache
sudo rm /etc/apache2/sites-available/000-default.conf
# Array with the lines of the new 000-default.conf
declare -a txt_array=(
"<VirtualHost *:80>"
"ServerAdmin webmaster@localhost"
"DocumentRoot /var/www/html"
"ErrorLog /var/log/apache2/error.log"
"CustomLog /var/log/apache2/access.log combined"
"Alias /static $proj_dir/assets/static"
"<Directory $proj_dir/assets/static>"
"Require all granted"
"</Directory>"
"<Directory $proj_dir/$django_proj_name>"
"<Files wsgi.py>"
"Require all granted"
"</Files>"
"</Directory>"
"WSGIDaemonProcess $django_proj_name python-home=$proj_dir/$python_env python-path=$proj_dir"
"WSGIProcessGroup $django_proj_name"
"WSGIScriptAlias / $proj_dir/$django_proj_name/wsgi.py"
"SetEnv STHOME $proj_dir"
"SetEnv STLANG en_US.UTF-8"
"SetEnv STLANGUAGE en_US"
"</VirtualHost>"
)
# Write the new 000-default.conf
for i in "${txt_array[@]}"
do
echo -e "$i" | sudo tee --append /etc/apache2/sites-available/000-default.conf
done
# Restart apache2
sudo ufw allow 'Apache Full'
sudo service apache2 restart
# Give permissions to the Apache2 user
sudo setfacl -R -m u:www-data:rwX $proj_dir
sudo setfacl -dR -m u:www-data:rwX $proj_dir
# Do migrations
cd $proj_dir && $proj_dir/$python_env/bin/python manage.py makemigrations
cd $proj_dir && $proj_dir/$python_env/bin/python manage.py migrate
# Collect static
sudo rm -r $proj_dir/assets/static
cd $proj_dir && $proj_dir/$python_env/bin/python manage.py collectstatic
sudo service apache2 restart