"""
WSGI config for cw_mdt project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
import sys

from cw_mdt.settings import *

PROJECT_DIR = os.path.abspath(os.path.join(BASE_DIR, '..'))

sys.path.append(PROJECT_DIR)

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cw_mdt.settings")

application = get_wsgi_application()

import django.core.handlers.wsgi
_application = django.core.handlers.wsgi.WSGIHandler()


def application(environ, start_response):
	try:
		os.environ['HOME']     = environ['STHOME']
		os.environ['LANG']     = environ['STLANG']
		os.environ['LANGUAGE'] = environ['STLANGUAGE']
	except:
		os.environ['HOME']     = '/home/user'
		os.environ['LANG']     = 'pt_PT'
		os.environ['LANGUAGE'] = 'pt'
	return _application(environ, start_response)