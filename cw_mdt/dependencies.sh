#!/bin/bash
sudo apt-get update
sudo apt-get install openssh-server
# Install Python, pip and virtualenv
sudo apt-get install software-properties-common
sudo apt-get install python-software-properties
sudo apt-get install python-pip
sudo -H pip install --upgrade pip
sudo -H pip install virtualenv
# Install Apache and apache2 extensions
sudo apt-get install apache2 libapache2-mod-wsgi
# Install nodejs, npm and bower
sudo apt-get install nodejs
sudo apt-get install npm
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo npm install bower -g
# Install GDAL and GRASS GIS
sudo apt-get install gdal-bin libgdal-dev python-gdal
#sudo apt-get install libgdal20
sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sudo apt-get update
sudo apt-get install grass grass-dev
# Install geoserver
sudo apt-get install tomcat7 tomcat7-admin
sudo apt-get install unzip
sudo mkdir /home/user/geoserver_inst
sudo wget http://sourceforge.net/projects/geoserver/files/GeoServer/2.10.4/geoserver-2.10.4-war.zip -P /home/user/geoserver_inst
sudo unzip /home/user/geoserver_inst/geoserver-2.10.4-war.zip -d /home/user/geoserver_inst/
sudo mv /home/user/geoserver_inst/geoserver.war /var/lib/tomcat7/webapps/
sudo a2enmod proxy proxy_ajp
sudo service apache2 restart
sudo service tomcat7 restart
# Install django
sudo -H pip install django==1.11.2
sudo -H pip install requests