"""
Tools for Geoserver layers management
"""


def publish_raster_layer(layername, datastore, workspace, epsg_code,
                         conf={'USER': 'admin', 'PASSWORD': 'geoserver',
                               'HOST': 'localhost', 'PORT': '8888'}):
    """
    Publish a Raster layer
    """
    
    import os
    import requests
    
    from .Xml import write_xml_tree
    from ..utils import get_random_string
    from ..utils import re_write_folder
    
    def get_wkt_web(epsg):
        r = requests.get(
            'http://spatialreference.org/ref/epsg/{}/ogcwkt/'.format(
                str(epsg)
            )
        )
        
        return r.text    
    
    url = (
        'http://{host}:{port}/geoserver/rest/workspaces/{work}/'
        'coveragestores/{storename}/coverages'
    ).format(
        host=conf['HOST'], port=conf['PORT'],
        work=workspace, storename=datastore
    )
    
    # Create obj with data to be written in the xml
    xmlTree = {
        "coverage" : {
            "name"  : layername,
            "title" : layername,
            "nativeCRS" : str(get_wkt_web(epsg_code)),
            "srs"   : 'EPSG:{}'.format(str(epsg_code)),
        }
    }
    
    # Write XML
    wTmp = re_write_folder(
        os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            get_random_string(7)
        )
    ) 
    
    xml_file = write_xml_tree(
        xmlTree, os.path.join(wTmp, 'rst_lyr.xml')
    )
    
    # Create layer
    with open(xml_file, 'rb') as f:
        r = requests.post(
            url, data=f,
            headers={'content-type': 'text/xml'},
            auth=(conf['USER'], conf['PASSWORD'])
        )
    
    return r

