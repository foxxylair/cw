"""
Tools for Geoserver datastores management
"""


def add_raster_store(raster, store_name, workspace,
                     conf={'USER': 'admin', 'PASSWORD': 'geoserver',
                           'HOST': 'localhost', 'PORT': '8888'}):
    """
    Create a new store with a raster layer
    """
    
    import os
    import requests
    
    from .Xml import write_xml_tree
    
    url = (
        'http://{host}:{port}/geoserver/rest/workspaces/{work}/'
        'coveragestores?configure=all'
    ).format(
        host=conf['HOST'], port=conf['PORT'],
        work=workspace
    )
    
    # Create obj with data to be written in the xml
    xmlTree = {
        "coverageStore" : {
            "name"   : store_name,
            "workspace": workspace,
            "enabled": "true",
            "type"   : "GeoTIFF",
            "url"    : raster
        }
    }
    
    treeOrder = {
        "coverageStore" : ["name", "workspace", "enabled", "type", "url"]
    }
    
    # Write XML
    xml_file = write_xml_tree(
        xmlTree,
        os.path.join(os.path.dirname(raster), 'new_store.xml'),
        nodes_order=treeOrder
    )
    
    # Send request
    with open(xml_file, 'rb') as f:
        r = requests.post(
            url,
            data=f,
            headers={'content-type': 'text/xml'},
            auth=(conf['USER'], conf['PASSWORD'])
        )
        
    return r

