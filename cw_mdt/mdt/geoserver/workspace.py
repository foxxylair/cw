"""
Tools for Geoserver workspaces management
"""

def create_workspace(name, 
                     conf={
                         'USER':'admin', 'PASSWORD': 'geoserver',
                         'HOST':'localhost', 'PORT': '8080'
                         }):
    """
    Create a new Geoserver Workspace
    """

    import requests
    import json

    url = 'http://{host}:{port}/geoserver/rest/workspaces'.format(
        host=conf['HOST'], port=conf['PORT']
    )

    r = requests.post(
        url,
        data=json.dumps({'workspace': {'name':name}}),
        headers={'content-type': 'application/json'},
        auth=(conf['USER'], conf['PASSWORD'])
    )

    return r

