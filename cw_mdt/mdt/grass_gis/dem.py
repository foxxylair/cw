"""
DEM Construction
"""

def make_DEM(grass_workspace, data, field, output, extent_template):
    from . import run_grass
    
    # Create GRASS GIS Location
    grass_base = run_grass(
        grass_workspace, 'gr_loc', 3763
    )
    
    # Start GRASS GIS Session
    import grass.script as grass
    import grass.script.setup as gsetup
    gsetup.init(grass_base, grass_workspace, 'gr_loc', 'PERMANENT')
    
    # Configure region
    from .tools import convert
    from .tools import raster_to_region
    convert(extent_template, 'extent')
    raster_to_region('extent')
    
    # Convert elevation "data" to GRASS Vector
    convert(data, 'elevation')
    
    # Elevation (GRASS Vector) to Raster
    from .tools import shp_to_raster
    shp_to_raster('elevation', 'rst_elevation', field)
    
    # Multiply cells values by 100 000.0
    from .tools import mapcalc
    mapcalc('int(rst_elevation * 100000)', 'rst_elev_int')
    
    # Run IDW to generate the new DEM
    from .tools import ridw
    ridw('rst_elev_int', 'dem_int', numberPoints=15)
    
    # DEM to Float
    mapcalc('dem_int / 100000.0', 'final_dem')
    
    # Export DEM to a file outside GRASS Workspace
    convert('final_dem', output)
    
    return output

