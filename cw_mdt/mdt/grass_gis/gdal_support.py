import os

from osgeo import gdal
from osgeo import ogr


def get_driver_name(shp):
    """
    Return the driver for a given file format
    """
    drv = {
        '.gml' : 'GML',
        '.shp': 'ESRI Shapefile',
        '.json': 'GeoJSON',
        '.kml': 'KML',
        '.osm': 'OSM',
        '.dbf': 'ESRI Shapefile',
        '.vct': 'Idrisi',
        '.nc': 'netCDF',
        '.vrt': 'VRT',
        '.mem': 'MEMORY'
    }
    name, ext = os.path.splitext(shp)
    if not ext:
        raise 'The file {} does not have extension'.format(shp)
    return drv[ext]

def gdal_get_driver_name(url):
    """
    Return the driver for a given file format
    """
    drv = {
        '.tif' : 'GTiff',
        '.ecw' : 'ECW',
        '.mpr' : 'ILWIS', '.mpl' : 'ILWIS',
        '.jpg': 'JPEG',
        '.nc': 'netCDF',
        '.png': 'PNG',
        '.vrt': 'VRT'
    }
    
    name, ext = os.path.splitext(url)
    return drv[ext]


def gdal_shp_to_raster(shp, cellsize, nodata, rst):
    """
    Convert a vectorial file to raster
    """
    
    # Get Extent
    dtShp = ogr.GetDriverByName(get_driver_name(shp)).Open(shp, 0)
    lyr = dtShp.GetLayer()
    srs = lyr.GetSpatialRef()
    x_min, x_max, y_min, y_max = lyr.GetExtent()
    
    # Create Output
    x_res = int((x_max - x_min) / cellsize)
    y_res = int((y_max - y_min) / cellsize)
    
    dtRst = gdal.GetDriverByName(
        gdal_get_driver_name(rst)).Create(rst, x_res, y_res, gdal.GDT_Byte)
    
    dtRst.SetGeoTransform((x_min, cellsize, 0, y_max, 0, -cellsize))
    bnd = dtRst.GetRasterBand(1)
    bnd.SetNoDataValue(nodata)
    
    gdal.RasterizeLayer(dtRst, [1], lyr, burn_values=[1])
    return rst


def shpextent_to_boundary(inShp, outShp, outRaster=None, cellsize=None):
    """
    Read one feature class extent and create a boundary with that
    extent
    
    The outFile could be a Feature Class or one Raster Dataset
    """
    
    def copy_file(src, dest):
        """
        Copy a file
        """
    
        from shutil import copyfile
    
        copyfile(src, dest)
    
        return dest
    
    def get_extent(shp):
        """
        Return extent of a Vectorial file
        
        Return a tuple object with the follow order:
        (left, right, bottom, top)
        """
        
        from decimal import Decimal
        
        dt = ogr.GetDriverByName(get_driver_name(shp)).Open(shp, 0)
        lyr = dt.GetLayer()
        extent = lyr.GetExtent()
        
        dt.Destroy()
        
        return [Decimal(x) for x in extent]
    
    def create_point(x, y):
        """
        Return a OGR Point geometry object
        """
        
        pnt = ogr.Geometry(ogr.wkbPoint)
        pnt.AddPoint(float(x), float(y))
        
        return pnt
    
    extent = get_extent(inShp)
    
    # Create points of the new boundary based on the extent
    boundary_points = [
        create_point(extent[0], extent[3]),
        create_point(extent[1], extent[3]),
        create_point(extent[1], extent[2]),
        create_point(extent[0], extent[2]),
        create_point(extent[0], extent[3])
    ]
    
    # Write new file
    shp = ogr.GetDriverByName(
        get_driver_name(outShp)).CreateDataSource(outShp)
    
    lyr = shp.CreateLayer(
        'extent',
        geom_type=ogr.wkbPolygon
    )
    
    outDefn = lyr.GetLayerDefn()
    
    feat = ogr.Feature(outDefn)
    ring = ogr.Geometry(ogr.wkbLinearRing)
    
    for pnt in boundary_points:
        ring.AddPoint(pnt.GetX(), pnt.GetY())
    
    polygon = ogr.Geometry(ogr.wkbPolygon)
    polygon.AddGeometry(ring)
    
    feat.SetGeometry(polygon)
    lyr.CreateFeature(feat)
    
    feat.Destroy()
    shp.Destroy()
    
    # Copy prj
    if os.path.isfile(os.path.splitext(inShp)[0] + '.prj'):
        copy_file(
            os.path.splitext(inShp)[0] + '.prj',
            os.path.splitext(outShp)[0] + '.prj'
        )
    
    if outRaster:
        cellsize = 10 if not cellsize else cellsize
        
        gdal_shp_to_raster(outShp, cellsize, -1, outRaster)


def gdal_translate(inRst, outRst):
    """
    Conversion between raster formats
    """

    import subprocess

    outDrv = gdal_get_driver_name(outRst)
    cmd = 'gdal_translate -of {drv} {_in} {_out}'.format(
        drv=outDrv, _in=inRst, _out=outRst
    )

    p = subprocess.Popen(cmd, shell=True,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    out, err = p.communicate()

    print out
    print err
    print p.returncode

    if p.returncode != 0:
        raise ValueError(
            'Output: {o}; ERROR: {e}'.fomat(o=out, e=err)
        )

