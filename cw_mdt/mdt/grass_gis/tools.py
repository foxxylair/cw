"""
GRASS GIS Tools converted to Python
"""

from grass.pygrass.modules import Module

def convert(inLyr, outLyr, geom_type=None):
    """
    Add or export data from/to GRASS GIS
    """
    
    import os
    
    def VectorialDrivers():
        return {
            '.shp': 'ESRI_Shapefile',
        }
    
    def RasterDrivers():
        return {
            '.tif': 'GTiff',
            '.img': 'HFA'
        }
    
    if os.path.splitext(inLyr)[1] in VectorialDrivers():
        inOrOut = 'import'
        data = 'vector'
    
    elif os.path.splitext(inLyr)[1] in RasterDrivers():
        inOrOut = 'import'
        data = 'raster'
    
    else:
        outFormat = os.path.splitext(outLyr)[1]
        inOrOut = 'export'
        if outFormat in VectorialDrivers():    
            data = 'vector'
        elif outFormat in RasterDrivers():
            data = 'raster'
        else:
            raise ValueError(
                "Not possible to identify if you want export or import data"
            )
    
    #*************************************************************************#
    if data == 'vector':
        if inOrOut == 'import':
            m = Module('v.in.ogr', input=inLyr, output=outLyr, flags='o',
                       overwrite=True, run_=False, quiet=True)
        elif inOrOut == 'export':
            m = Module('v.out.ogr', input=inLyr, type=geom_type, output=outLyr,
                       format=VectorialDrivers()[outFormat],
                       overwrite=True, run_=False, quiet=True)
    
    elif data == 'raster':
        if inOrOut == 'import':
            m = Module("r.in.gdal", input=inLyr, output=outLyr, flags='o',
                       overwrite=True, run_=False, quiet=True)
        elif inOrOut == 'export':
            m = Module("r.out.gdal", input=inLyr, output=outLyr,
                       format=RasterDrivers()[outFormat],
                       overwrite=True, run_=False, quiet=True)
    
    m()


def shp_to_raster(shp, rst, source):
    """
    Vectorial geometry to raster
    
    If source is None, the convertion will be based on the cat field.
    
    If source is a string, the convertion will be based on the field
    with a name equal to the given string.
    
    If source is a numeric value, all cells of the output raster will have
    that same value.
    """
    
    if source == None:
        m = Module(
            'v.to.rast', input=shp, output=rst, use="cat",
            overwrite=True, run_=False, quiet=True
        )
    
    elif type(source) == str or type(source) == unicode:
        m = Module(
            'v.to.rast', input=shp, output=rst, use="attr",
            attribute_column=source, overwrite=True, run_=False, quiet=True
        )
    
    elif type(source) == int or type(source) == float:
        m = Module(
            'v.to.rast', input=shp, output=rst, use="val",
            value=source, overwrite=True, run_=False, quiet=True
        )
    
    else:
        raise ValueError('\'source\' parameter value is not valid')
    
    m()


def mapcalc(equation, out):
    rc = Module('r.mapcalc',
                '{o} = {e}'.format(o=out, e=equation),
                overwrite=True, run_=False, quiet=True)
    rc()


def ridw(inRst, outRst, numberPoints=None):
    """
    r.surf.idw - Provides surface interpolation from raster point data
    by Inverse Distance Squared Weighting.
    
    r.surf.idw fills a grid cell (raster) matrix with interpolated values
    generated from input raster data points. It uses a numerical approximation
    technique based on distance squared weighting of the values of nearest data
    points. The number of nearest data points used to determined the
    interpolated value of a cell can be specified by the user (default:
    12 nearest data points).
    
    If there is a current working mask, it applies to the output raster map.
    Only those cells falling within the mask will be assigned interpolated
    values. The search procedure for the selection of nearest neighboring
    points will consider all input data, without regard to the mask.
    The -e flag is the error analysis option that interpolates values
    only for those cells of the input raster map which have non-zero
    values and outputs the difference (see NOTES below).
    
    The npoints parameter defines the number of nearest data points used to
    determine the interpolated value of an output raster cell.
    """
    
    numberPoints = 12 if not numberPoints else numberPoints
    
    idw = Module(
        'r.surf.idw', input=inRst, output=outRst, npoints=numberPoints,
        run_=False, quiet=True, overwrite=True
    )
    
    idw()


def raster_to_region(__raster):
    from grass.pygrass.modules import Module
    r = Module(
        "g.region", raster=__raster, run_=False, quiet=True
    )
    
    r()

