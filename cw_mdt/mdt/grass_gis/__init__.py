"""
GRASS GIS Python tools - Start GRASS GIS Session with Python
"""


import os
import shutil
import sys

def execute_cmd(cmd):
    """
    Execute a command and provide information about the results
    """
    import subprocess
    
    p = subprocess.Popen(cmd, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
    out, err = p.communicate()
    
    return p.returncode, out, err


def open_grass_linux(gisdb, location, srs):
    """
    Method to open GRASS GIS on Linux Systems
    
    Parameters:
    * gisdb - abs path to grass workspace
    * location - name for the grass location
    * srs - epsg or file to define spatial reference system of the location that 
    will be created
    """    
    # Delete location if exists
    if os.path.exists(os.path.join(gisdb, location)):
        shutil.rmtree(os.path.join(gisdb, location))
    
    grassbin = 'grass72'
    startcmd = grassbin + ' --config path'
    
    code, out, err = execute_cmd(startcmd)
    if code != 0:
        print (
            'Kind: OPEN GRASS on get config path\n'
            'Output: {o} \nError: {e}'
        ).format(
            o=str(out), e=str(err)
        )
        sys.exit(-1)
    
    gisbase = out.strip('\n')
    # Set GISBASE environment variable
    os.environ['GISBASE'] = gisbase
    # the following not needed with trunk
    os.environ['PATH'] += os.pathsep + os.path.join(gisbase, 'extrabin')
    # add path to GRASS addons
    home = os.path.expanduser("~")
    os.environ['PATH'] += os.pathsep + os.path.join(home, '.grass7', 'addons', 'scripts')    
    # define GRASS-Python environment
    gpydir = os.path.join(gisbase, "etc", "python")
    sys.path.append(gpydir)
    location_path = os.path.join(gisdb, location)
    if type(srs) == type(1):
        startcmd = grassbin + ' -c epsg:' + str(srs) + ' -e ' + location_path
    elif type(srs) == type('string'):
        startcmd = grassbin + ' -c ' + srs + ' -e ' + location_path
    
    code, out, err = execute_cmd(startcmd)
    if code != 0:
        print (
            'Kind: OPEN GRASS on location creation\n'
            'Output: {o}\n'
            'Error: {e}'
        ).format(
            o=str(out), e=str(err)
        )
        sys.exit(-1)
    
    # Set GISDBASE environment variable
    os.environ['GISDBASE'] = gisdb
    
    # See if there is location
    if not os.path.exists(os.path.join(gisdb, location)):
        print 'NoError, but location is not created'
        sys.exit(-1)
    
    return gisbase



def open_grass_windows(gisdb, location, srs,
                       grass_path_win=r'C:\GRASS_GIS_7\grass72.bat'):
    """
    Method to open GRASS GIS on MS Windows Systems
    
    Parameters:
    * gisdb - abs path to grass workspace
    * location - name for the grass location
    * srs - epsg or file to define spatial reference system of the location that 
    will be created
    * grass_path_win - abs path to the GRASS GIS executable file (bat file)
    """    
    # Delete location if exists
    if os.path.exists(os.path.join(gisdb, location)):
        shutil.rmtree(os.path.join(gisdb, location))
    
    # the path to grass can't have white spaces
    startcmd = grass_path_win + ' --config path'
    code, out, err = execute_cmd(startcmd)
    if code != 0:
        print (
            'Kind: OPEN GRASS on get config path\n'
            'Output: {o} \nError: {e}'
            ).format(
                o=str(out), e=str(err)
            )
        sys.exit(-1)
    
    # Set GISBASE environment variable
    gisbase = out.strip().split('\r\n')[0]
    os.environ['GRASS_SH'] = os.path.join(gisbase, 'msys', 'bin', 'sh.exe')
    os.environ['GISBASE'] = gisbase
    # define GRASS-Python environment
    gpydir = os.path.join(gisbase, "etc", "python")
    sys.path.append(gpydir)
    
    # define database location
    location_path = os.path.join(gisdb, location)
    if type(srs) == int:
        startcmd = '{gb} -c epsg:{e} -e {lp}'.format(
            gb = grass_path_win, e = srs, lp = location_path
        )
    elif type(srs) == type('string'):
        startcmd = '{gb} -c {extf} -e {lp}'.format(
            gb = grass_path_win, extf = srs, lp = location_path
        )
    
    # open grass
    code, out, err = execute_cmd(startcmd)
    
    if code != 0:
        print (
            'Kind: OPEN GRASS on location creation\n'
            'Output: {o}\n'
            'Error: {e}'
            ).format(
                o=str(out), e=str(err)
            )
        sys.exit(-1)
    
    # Set GISDBASE environment variable
    os.environ['GISDBASE'] = gisdb
    return gisbase


def run_grass(workspace, location, srs, win_path=None):
    """
    Generic method that could be used to put GRASS GIS running in any Os
    """
    
    def os_name():
        import platform
        return str(platform.system())
    
    __os = os_name()
    
    if __os == 'Linux':
        base = open_grass_linux(workspace, location, srs)
    
    elif __os == 'Windows':
        if not win_path:
            base = open_grass_windows(workspace, location, srs)
        else:
            base = open_grass_windows(
                workspace, location, srs, grass_path_win=win_path
            )
    
    else:
        raise ValueError('Could not identify operating system')
    
    return base

