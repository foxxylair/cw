# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
def redirect(request):
    return HttpResponseRedirect('/dem/create')

def dem_inputs(request):
    #Import Form
    from .forms import DemInputsForm
    
    #Assign form class to a pyobj
    form = DemInputsForm()

    return render(
        request, 'mdt/dem_inputs.html',
        {'form' : form}
    )

def dem_result(request):
    """
    Django View ready to create DEM based on several input parameters
    """
    
    # Import Form
    from .forms import DemInputsForm
    
    if request.method == 'POST':
        """
        Process input parameters for DEM Creation
        """
        import os
        from .utils import save_geodata
        from .utils import get_random_string
        from .utils import re_write_folder
        from .grass_gis.dem import make_DEM
        from .grass_gis.gdal_support import shpextent_to_boundary
        
        form = DemInputsForm(request.POST, request.FILES)
        
        # Receive Elevation Data
        data_folder = re_write_folder(os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'DEM_DATA',
            get_random_string(7)
        ))
        
        elevation = save_geodata(request, 'elevation', data_folder)
        
        # Receive Name of the Field with Elevation Data
        field_name = str(request.POST['elev_field'])
        
        # Receive cellsize
        cellsize = int(request.POST['cellsize'])
        
        # Receive format for the output file
        out_format = str(request.POST['output_format'])
        
        # Create a raster file that will be used to define the cellsize and
        # extent of the GRASS GIS workspace
        shpextent_to_boundary(
            elevation,
            os.path.join(data_folder, 'extent.shp'),
            outRaster=os.path.join(data_folder, 'extent.tif'),
            cellsize=cellsize
        )

        outDEM = os.path.join(data_folder, 'dem' + out_format)
        make_DEM(
            data_folder, elevation, field_name,
            outDEM,
            os.path.join(data_folder, 'extent.tif')
        )

        # Send Result to Geoserver
        from .geoserver.workspace import create_workspace
        from .geoserver.stores import add_raster_store
        from .geoserver.layers import publish_raster_layer

        GEOSERVER_CONNECTION_PARAMETERS = {
            'HOST': 'localhost', 'PORT': '8080',
            'USER': 'admin', 'PASSWORD': 'geoserver'
        }

        if out_format == '.img':
            from .grass_gis.gdal_support import gdal_translate
            geoTIF = os.path.join(data_folder, 'dem.tif')

            gdal_translate(outDEM, geoTIF)

        else:
            geoTIF = outDEM

        workspace_name = os.path.basename(data_folder)
        create_workspace(workspace_name, conf=GEOSERVER_CONNECTION_PARAMETERS)
        add_raster_store(
            geoTIF, 'dem_store',
            workspace_name, conf=GEOSERVER_CONNECTION_PARAMETERS
        )
        publish_raster_layer(
            'dem_lyr', 'dem_store', workspace_name, 3763,
            conf=GEOSERVER_CONNECTION_PARAMETERS
        )

        # For now, render outputs in HTML
        return render(
            request, 'mdt/dem_results.html',
            {
                'elevation_path' : elevation,
                'field'          : field_name,
                'cellsize'       : cellsize,
                'out_format'     : out_format,
                'foldername'     : os.path.basename(data_folder),
                'filename'       : 'dem',
                'fileformat'     : out_format[1:],
                'geoserver_w'    : workspace_name,
                'geoserver_lyr'  : 'dem_lyr'
            }
        )
    
    else:
        """
        For some reason, someone reached this URL without providing
        input parameters for a DEM creation... Redirecting to the home
        URL
        """
        
        return HttpResponseRedirect('/dem/create/')


def download_dem(request, random_str, fname, fformat):
    import os
    from django.http import HttpResponse
    
    file_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'DEM_DATA',
        random_str,
        fname + '.' + fformat
    )
    
    with open(file_path, 'rb') as f:
        response = HttpResponse(f.read())
        response['content_type'] = 'image/' + fformat
        response['Content-Disposition'] = 'attachment;filename={n}.{e}'.format(
            n=str(fname), e=str(fformat))
    
    return response

