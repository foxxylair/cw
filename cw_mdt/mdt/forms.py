from django import forms

class DemInputsForm(forms.Form):
	
	elevation = forms.FileField(
		label="Upload Elevation Data",
		widget=forms.ClearableFileInput(attrs={'multiple':True})
	)

	elev_field = forms.CharField(
		label='Name of the field with elevation data',
		max_length=11
	)

	cellsize = forms.CharField(
		label='Output cellsize',
		max_length=4
	)

	output_format = forms.ChoiceField(
		choices=(
			('.tif', 'GeoTiff'),
			('.img', 'Erdas Imagine')
			)
		)