from django.conf.urls import url

from . import views

urlpatterns = [
    # URL to redirect if url is not /dem/create
    url(r'^$', views.redirect, name='redirect'),
    # URL to page with input parameters
    url(r'^create/$', views.dem_inputs,name = 'dem_inputs'),
    # URL to see results
    url(r'^result/$', views.dem_result, name='dem_result'),
    url(r'^down/(?P<random_str>\w+)/(?P<fname>\w+)/(?P<fformat>\w+)/$',
    	views.download_dem,
    	name='dem_download')
]