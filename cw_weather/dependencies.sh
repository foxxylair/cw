#!/bin/bash
sudo apt-get update
sudo apt-get install openssh-server
# Install Python, pip and virtualenv
sudo apt-get install software-properties-common
sudo apt-get install python-software-properties
sudo apt-get install python-pip
sudo -H pip install --upgrade pip
sudo -H pip install virtualenv
# Install Apache and apache2 extensions
sudo apt-get install apache2 libapache2-mod-wsgi
# Install nodejs, npm and bower
sudo apt-get install nodejs
sudo apt-get install npm
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo npm install bower -g
# Install GDAL
sudo apt-get install gdal-bin libgdal-dev python-gdal
# Install django
sudo -H pip install django==1.11.2
sudo -H pip install requests