"""
Some utilities that will support Views Activity
"""

def get_random_string(char_number, all_char=None):
    """
    Generates a random string with numbers and characters
    """
    
    import random as r
    import string
    
    char = string.digits + string.ascii_letters
    if all_char:
        char += string.punctuation
    
    rnd = ''
    
    for i in range(char_number): rnd += r.choice(char)
    
    return rnd


def re_write_folder(folder):
    """
    Create a new folder
    Replace the given folder if that one exists
    """
    import os
    
    if os.path.exists(folder):
        shutil.rmtree(folder)
    os.mkdir(folder)
    return folder


def save_geodata(request, field_tag, folder):
    """
    Receive a file with vectorial geometry from a form field:
    
    Store the file in the server
    
    IMPORTANT: this method will only work if the FORM that is receiving the 
    files allows multiple files
    """
    
    import os
    
    def save_file(save_fld, _file):
        """
        Store a uploaded file in a given folder
        """ 

        with open(os.path.join(save_fld, _file.name), 'wb+') as destination:
            for chunk in _file.chunks():
                destination.write(chunk)
    
    files = request.FILES.getlist(field_tag)
    
    for f in files:
        save_file(folder, f)
    
    if len(files) > 1:
        shape = os.path.join(folder, '{f_name}.shp'.format(
            f_name = os.path.splitext(files[0].name)[0]
        ))
    
    else:
        shape = os.path.join(folder, files[0].name)
    
    return shape

