# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect

def redirect(request):
    return HttpResponseRedirect('/weather/create/')

def weather_inputs(request):
    #Import Form
    from .forms import WeatherInputsForm

    #Assign form class to a pyobj
    form = WeatherInputsForm()

    return render(
        request, 'weather/weather_inputs.html',
        {'form' : form}
    )

def weather_result(request):
    from .forms import WeatherInputsForm

    if request.method == 'POST':
        import os
        from .geolookup import to_shape
        from .geolookup import ogr_btw_driver
        from .utils import get_random_string
        from .utils import re_write_folder

        """
        Process input parameters for Weather response
        """

        form = WeatherInputsForm(request.POST)

        data_folder = re_write_folder(os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'WEATHER_DATA',
            get_random_string(7)
        ))

        # Receive Name of the Field with latitude data
        lat = str(request.POST['latitude'])
        # Receive Name of the Field with longitude Data
        lng = str(request.POST['longitude'])

        CLIMATIC_SHAPE = os.path.join(data_folder, 'climatic_data.shp')
        to_shape(lat,lng, CLIMATIC_SHAPE)
        CLIMATIC_JSON = os.path.join(data_folder, 'climatic_data.json')
        ogr_btw_driver(CLIMATIC_SHAPE, CLIMATIC_JSON)

        return render(
            request, 'weather/weather_results.html',
            {
                'random_str' : os.path.basename(data_folder),
                'filename' : os.path.splitext(os.path.basename(CLIMATIC_JSON))[0]
            }
        )

def jsonfile_to_response(request, random_str, filename):
    """
    Return geodata in a JSON file as JSON Response
    """

    from django.http import JsonResponse
    import os
    import json

    json_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'WEATHER_DATA',
        random_str, filename + '.json'
    )

    with open(json_file, mode='r') as f:
        data = json.load(f)

    return JsonResponse(data, safe=False)


