from django.conf.urls import url

from . import views

urlpatterns = [
	# URL to redirect if url is not /weather/create
    url(r'^$', views.redirect, name='redirect'),
    # URL to page with input parameters
	url(r'^create/$', views.weather_inputs, name= 'weather_inputs'),
	#URL results
	url(r'^result/$', views.weather_result, name='weather_result'),
	# Climatic data as JSON Response
	url(r'^climatic_data/(?P<random_str>\w+)/(?P<filename>\w+)/$', views.jsonfile_to_response, name='clime_data_json')
]