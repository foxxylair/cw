WEATHER_URL = 'https://api.wunderground.com/api/'
WEATHER_UNDER_KEY = '0d520fc830c726a1'

def to_json(url):
    #Return json object with the json data available in the URL
    import urllib2
    import json
    
    f = urllib2.urlopen(url)
    read_page = f.read()
    json_data = json.loads(
        read_page.decode('utf-8', 'ignore'), encoding='utf-8'
    )
    
    return json_data

def geolookup_by_name(city,country):
    """
    Return json with stations found using as keywords the country and city names
    
    Portugal codes - PT; PO
    """
    data = to_json((
        '{url}{key}/geolookup/q/{coun}/{_city}.json').format(
        url=WEATHER_URL, key=WEATHER_UNDER_KEY,
        coun=country, _city=city
        )
    ) 
      
    return data

def geolookup_by_position(lat,lng):
    """
    Returns json with stations found using lat and long coordinates
    """
    data = to_json((
        '{url}{key}/geolookup/q/{__lat},{_long}.json').format(
            url=WEATHER_URL, key=WEATHER_UNDER_KEY,
            __lat=str(lat), _long=str(lng)
        )
    )
    
    return data

def conditions(lat, lng):
    """
    Return Weather Conditions JSON for one station 
    """
    
    data = to_json((
        '{url}{key}/geolookup/conditions/q/{_lat},{_lng}.json').format(
            url=WEATHER_URL, key=WEATHER_UNDER_KEY,
            _lat=str(lat), _lng=str(lng)
        )   
    )
    
    return data

def obtain_stations_conditions(lat, lng):
    #Obtain nearby sensors from lat,long

    data = geolookup_by_position(lat, lng)

    rawsensors = data['location']['nearby_weather_stations']
    sensors = []

    for k in rawsensors:

        for sensor in rawsensors[k]['station']:
            if sensor['lat'] and sensor['lon']:
                sensors.append((sensor['lat'], sensor['lon']))
            
            else:
                continue
    
    #Obtain climatic_data from each station found with def conditions
    
    climatic_data = []
    for station in sensors:
    
        d = conditions(station[0], station[1])
        
        climatic_data.append(d['current_observation'])
    
    d = conditions(lat, lng)
    climatic_data.append(d['current_observation'])
                
    return climatic_data

def to_shape(lat,lng, outShp):
    data = obtain_stations_conditions(lat,lng)
    
    import osgeo.ogr as ogr
    import osgeo.osr as osr
    import unicodedata
    
       
    # set up the shapefile driver
    driver = ogr.GetDriverByName("ESRI Shapefile")
    
    # create the data source
    data_source = driver.CreateDataSource(outShp)
    
    # create the spatial reference, WGS84
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    
    # create the layer
    layer = data_source.CreateLayer("climatic_data", srs, ogr.wkbPoint)
    
    # Add the fields we're interested in
    field_name = ogr.FieldDefn("location", ogr.OFTString)
    field_name.SetWidth(24)
    layer.CreateField(field_name)
    
    field_region = ogr.FieldDefn("Relhumidity", ogr.OFTString)
    field_region = ogr.FieldDefn("Weather", ogr.OFTString)
    field_region.SetWidth(24)
    layer.CreateField(field_region)

    layer.CreateField(ogr.FieldDefn("Elevation", ogr.OFTReal))
    layer.CreateField(ogr.FieldDefn("Temp_c", ogr.OFTReal))
    layer.CreateField(ogr.FieldDefn("Obs_time", ogr.OFTString))
    layer.CreateField(ogr.FieldDefn("Wind_kph", ogr.OFTInteger))
    
        
    # Process the text file and add the attributes and features to the shapefile
    for i in data:
        # create the feature
        feature = ogr.Feature(layer.GetLayerDefn())
        # Set the attributes using the values from the delimited text file
        location = unicodedata.normalize('NFKD', i['observation_location']['city']).encode('ascii', 'ignore')
        feature.SetField("Location", location)
        feature.SetField("Elevation", float(i['display_location']['elevation']))
        feature.SetField("Temp_c", float(i['temp_c']))
        feature.SetField("Weather", str(i['weather']))
        feature.SetField("Obs_time", str(i['observation_time']))
        feature.SetField("Wind_kph", int(i['wind_kph']))
        feature.SetField("Relhumidity", str(i['relative_humidity']))
        
        # create the WKT for the feature using Python string formatting
        wkt = "POINT(%f %f)" %  (float(i['display_location']['longitude']) , float(i['display_location']['latitude']))
      
        # Create the point from the Well Known Txt
        point = ogr.CreateGeometryFromWkt(wkt)
      
        # Set the feature geometry using the point
        feature.SetGeometry(point)
        # Create the feature in the layer (shapefile)
        layer.CreateFeature(feature)
        # Dereference the feature
        feature = None
    
    # Save and close the data source
    data_source = None    

def execute_cmd(cmd):
    '''
    Execute a command and provide info about the results
    '''
    import subprocess

    p = subprocess.Popen(cmd, 
        shell = True, 
        stdout = subprocess.PIPE,
        stderr = subprocess.PIPE)

    out, err = p.communicate()

    return p.returncode, out, err

def ogr_drivers():
    """
    OGR Drivers Repository
    Dict with the association between files formats and drivers
    """
    return {
        '.gml':'GML',
        '.shp':'ESRI Shapefile',
        '.json':'GeoJSON',
        '.kml':'KML',
        '.osm':'OSM',
        '.dbf':'ESRI Shapefile',
        '.vct':'Idrisi',
        '.nc':'netCDF',
        '.vrt':'VRT',
        '.mem':'MEMORY'
    }

def get_driver_name(shp):
    import os
    '''
    Return the driver for a given file format
    '''
    drv = ogr_drivers()
    name, ext = os.path.splitext(shp)
    if not ext:
        raise 'The file {} does not have extension'.format(shp)

    return drv[ext]

def ogr_btw_driver(inShp, outShp):
    '''
    Convert Vectorial file to another ogr driver
    '''
    import os
    import osgeo.ogr as ogr

    out_driver = get_driver_name(outShp)

    cmd= 'ogr2ogr -f "{drv}" {out} {_in}'.format(
        drv=out_driver, out=outShp, _in=inShp
    )

    code,out,err = execute_cmd(cmd)

    if code != 0:
        raise ValueError ('Out:{o} \n Error: {e}'.format(
            o=str(out), e=str(err)
        ))   