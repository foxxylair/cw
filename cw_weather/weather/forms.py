from django import forms

class WeatherInputsForm(forms.Form):
	latitude = forms.CharField(
		label="Latitude:",
		max_length=8
	)

	longitude = forms.CharField(
		label="Longitude:",
		max_length=8
	)

